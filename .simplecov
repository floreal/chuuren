require 'simplecov-rcov'

SimpleCov.start do
  add_filter ['/features/', '/spec/', '/bootstrap/']
  add_group 'Models', 'src/models'
  add_group 'Storage', 'src/storage'
  add_group 'Controllers', 'src/controllers'

  SimpleCov.formatter = SimpleCov::Formatter::RcovFormatter
end