Feature: Game
  As a Mahjong Player
  I need to play games
  In order to participate to the league

  @nominal
  Scenario: I have a name
    Given A player named "Akagi"
    And a new game
    When the player registers as "east"
    Then "east" player should be "Akagi"

  @nominal
  Scenario: Completing a regular game
    Given a new game
    When I register players:
      | position | name    |
      | east     | Akagi   |
      | south    | Tatsuya |
      | west     | Saki    |
      | north    | Washizu |
    And I set scores:
      | position | score  |
      | east     | 30_000 |
      | south    | 22_000 |
      | west     | 21_000 |
      | north    | 27_000 |
    Then game should be completed

  @nominal
  Scenario: Completing a san-nin game
    Given a new game
    And that game is a san nin
    When I register players:
      | position | name    |
      | east     | Akagi   |
      | south    | Tatsuya |
      | west     | Saki    |
    And I set scores:
      | position | score  |
      | east     | 32_000 |
      | south    | 22_000 |
      | west     | 21_000 |
    Then game should be completed