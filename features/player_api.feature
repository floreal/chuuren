Feature: Player API
  As a game manager
  I need an restful API for players
  In order to manage Players

  Background:
    Given Players in storage:
      | name    |
      | Akagi   |
      | Saki    |
      | Tatsuya |

  @api @database @nominal
  Scenario: Find All Players
    When calling "get" "/player/"
    Then I should get this json response:
      """
      [{"type":"Player","id":1,"name":"Akagi"},{"type":"Player","id":2,"name":"Saki"},{"type":"Player","id":3,"name":"Tatsuya"}]
      """

  @api @database @nominal
  Scenario Outline: Find a Player
    When calling "get" "/player/<id>"
    Then I should get this json response:
    """
    {"type":"Player","id":<id>,"name":"<name>"}
    """
    Examples:
      | id | name    |
      | 1  | Akagi   |
      | 2  | Saki    |
      | 3  | Tatsuya |


  @api @database @nominal
  Scenario Outline: Deleting a Player
    When calling "delete" "/player/<id>"
    Then I should get status code <202>
    And players should be removed:
      | id   | name   |
      | <id> | <name> |

    Examples:
      | id | name    |
      | 1  | Akagi   |
      | 2  | Saki    |
      | 3  | Tatsuya |

  @api @database @nominal
  Scenario: Create One Player
    When calling "post" "/player/" with params:
      | name    |
      | Washisu |
    Then I should get status code should be in:
      | status |
      | 302    |
      | 303    |
    And I should be redirected to "/player/4"
    And I should get this json response:
    """
    {"type":"Player","id":4,"name":"Washisu"}
    """
    And players should be
      | id | name    |
      | 1  | Akagi   |
      | 2  | Saki    |
      | 3  | Tatsuya |
      | 4  | Washisu |

  @api @database @nominal
  Scenario Outline: Updating a Player
    When calling "put" "/player/<id>" with params:
      | name    |
      | Washisu |
    Then I should get status code should be in:
      | status |
      | 302    |
      | 303    |
    And I should be redirected to "/player/<id>"
    And I should get this json response:
    """
    {"type":"Player","id":<id>,"name":"Washisu"}
    """

    Examples:
      | id |
      | 1  |
      | 2  |
      | 3  |

  @api @database @nominal
  Scenario: Searching user by name
    When calling "get" "/player/search/ak"
    Then I should get this json response:
      """
      [{"type":"Player","id":1,"name":"Akagi"},{"type":"Player","id":2,"name":"Saki"}]
      """

  @api @database @error_handling
  Scenario: Failing to add a player because name already exist
    When calling "post" "/player/" with params:
      | name  |
      | Akagi |
    Then I should get status code <409>

  @api @database @error_handling
  Scenario: Failing to get a player because none exits with given ID
    When calling "get" "/player/4"
    Then I should get status code <404>

  @api @database @error_handling
  Scenario: Failing to update a player because none exits with given ID
    When calling "put" "/player/4" with params:
      | name    |
      | Washisu |
    Then I should get status code <404>