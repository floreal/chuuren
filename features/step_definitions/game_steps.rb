include Chuuren::Models

Given(/^A player named "([^"]*)"$/) do |name|
  @player = new_player name: name
end

Given(/^a new game$/) do
  @game = Game::new
end

And(/^that game is a san nin$/) do
  @game.type = :san_nin
end

When(/^the player registers as "([^"]*)"$/) do |position|
  expect(@game.register position.to_sym, @player).to be_truthy
end

Then(/^"([^"]*)" player should be "([^"]*)"$/) do |position, name|
  expect(@game.who_is_at? position.to_sym).to eq name
end

When(/^I register players:$/) do |table|
  table.hashes.each do |row|
    expect(@game.register(row[:position].to_sym, new_player(row))).to be_truthy
  end
end

And(/^I set scores:$/) do |table|
  table.hashes.each do |row|
    expect(@game.set_score_for(row[:position].to_sym, row[:score].to_i)).to be_truthy
  end
end

Then(/^game should be completed$/) do
  expect(@game.complete?).to be_truthy
end