Given(/^Players in storage:$/) do |table|
  @starting_players = []
  table.hashes.each do |player|
    @starting_players << storage.create(new_player(player))
  end
end

When(/^calling "([^"]*)" "([^"]*)"$/) do |request_type, path|
  visit path, request_type
end

Then(/^I should get this json response:$/) do |expected_response|
  expect(JSON.parse response_body).to eq JSON.parse expected_response
end

Then(/^I should get status code <(\d+)>$/) do |status|
  expect(response_code).to eq status.to_i
end

And(/^players should be$/) do |plaxers|
  plaxers.map_column!(:id) { |id| id.to_i}
  expect(storage.all).to eq plaxers.hashes.map { |hash| new_player hash }
end

When(/^calling "([^"]*)" "([^"]*)" with params:$/) do |request_type, path, table|
  visit path, request_type, table.hashes.shift
end

Then(/^I should be redirected to "([^"]*)"$/) do |path|
  location = response.headers['Location']
  follow_redirect!
  expect(location).to match /#{path}$/
end

Then(/^I should get status code should be in:$/) do |table|
  allowed_code = table.hashes.map { |h| h[:status].to_i }
  expect(allowed_code).to include response_code
end


And(/^players should be removed:$/) do |player_data|
  player_data.map_column!(:id) { |id| id.to_i}
  deleted_player = player_data.hashes.map { |row| new_player row.to_h }.shift
  expected = @starting_players.reject { |player| deleted_player == player }
  actual = storage.all
  expect(actual).to eq expected
end