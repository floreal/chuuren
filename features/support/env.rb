require 'simplecov'

require File.dirname(__FILE__) + '/../..//bootstrap/wabapp.rb'

require 'rspec/expectations'
require 'rack/test'
require 'webrat'
require 'net/http'

Webrat.configure do |config|
  config.mode = :rack
end

class MyWorld
  include Rack::Test::Methods
  include Webrat::Methods
  include Webrat::Matchers
  include Chuuren

  def initialize
    setup_player_api
  end

  def new_player hash
    Models::Player.new hash
  end

  Webrat::Methods.delegate_to_session :response_code, :response_body

  def setup_player_api
    player_api = Controllers::PlayerAPI
    player_api.set :environment, :test
    player_api.set :database, SQLite3::Database::new("db/test.sqlite3")
    player_api.set :storage,  Storage::PlayerStorage::new(database)
  end

  def storage
    Controllers::PlayerAPI.settings.storage
  end

  def database
    Controllers::PlayerAPI.settings.database
  end

  def app
    @player_api = Rack::Builder.new do
      map '/player' do
        run Controllers::PlayerAPI
      end
    end
  end
end

def Rack.version
  "1.1"
end

After("@database") do
  database.execute("DELETE FROM player")
  database.execute("DELETE FROM sqlite_sequence where name='player'")
end

World{MyWorld.new}