describe Chuuren::Models::Game do
  before :each do
    @game = Chuuren::Models::Game::new
  end

  context 'positions' do
    it 'can be east' do
      expect(@game.position_exists? :east).to be_truthy
    end

    it 'can be south' do
      expect(@game.position_exists? :south).to be_truthy
    end

    it 'can be west' do
      expect(@game.position_exists? :west).to be_truthy
    end

    it 'can be north' do
      expect(@game.position_exists? :north).to be_truthy
    end

    it 'can\'t be any other position' do
      expect(@game.position_exists? :north_west).to be_falsey
    end

    context 'san-nin game' do
      it 'can\'t be north' do
        set_san_nin
        expect(@game.position_exists? :north).to be_falsey
      end
    end
  end

  context 'registering players' do
    before :each do
      @player = Chuuren::Models::Player::new name: 'Akagi'
    end

    it 'is alowed' do
      expect(@game.register :east, @player).to be_truthy
      expect(@game.players).to eq({east: 'Akagi'})
    end

    it 'is alowed only once' do
      @game.register :east, @player
      expect(@game.register :west, @player).to be_falsey
    end

    it 'is not alowed to register to a bad position' do
      expect(@game.register :north_west, @player).to be_falsey
    end

    it 'can tell who is at a position' do
      @game.register :east, @player
      expect(@game.who_is_at? :east).to eq 'Akagi'
    end

    context 'san-nin game' do
      it 'can\'t be north' do
        set_san_nin
        expect(@game.register :north, @player).to be_falsey
      end
    end
  end

  context 'scoring' do
    it 'can be set by position' do
      expect(@game.set_score_for :east, 25_000).to be_truthy
      expect(@game.scores).to eq({east: 25_000, south: 0, west: 0, north: 0})
    end

    it 'doesn\'t allow to set score for a non existent position' do
      expect(@game.set_score_for :north_west, 25_000).to be_falsey
    end

    context 'regular game' do
      it 'should complete a game if total scores is 100 000' do
        expect(@game.complete?).to be_falsey
        {
            east: 25_000,
            south: 25_000,
            west: 25_000,
            north: 25_000
        }.each { |position, score| @game.set_score_for position, score }
        expect(@game.complete?).to be_truthy
      end
    end

    context 'san-nin game' do
      before :each do
        set_san_nin
      end

      it 'can\'t have a score for north' do
        expect(@game.set_score_for :north, 25_000).to be_falsey
      end

      it 'should complete a game if total scores is 75 000' do
        expect(@game.complete?).to be_falsey
        {
            east: 25_000,
            south: 25_000,
            west: 25_000
        }.each { |position, score| @game.set_score_for position, score }

        expect(@game.complete?).to be_truthy
      end
    end
  end

  context 'umas' do
    it 'can be applied' do

      {
          east: 26_000,
          south: 30_000,
          west: 20_000,
          north: 24_000
      }.each { |position, score| @game.set_score_for position, score }
      expect(@game.apply_uma!).to be_truthy
      expect(@game.scores).to eq({
                                     east: 31_000,
                                     south: 40_000,
                                     west: 10_000,
                                     north: 19_000
                                 })
    end

    it 'is applied only once' do

      {
          east: 26_000,
          south: 30_000,
          west: 20_000,
          north: 24_000
      }.each { |position, score| @game.set_score_for position, score }
      expect(@game.apply_uma!).to be_truthy
      expect(@game.apply_uma!).to be_falsey
      expect(@game.scores).to eq({
                                     east: 31_000,
                                     south: 40_000,
                                     west: 10_000,
                                     north: 19_000
                                 })
    end

    context 'san-nin game' do
      it 'is also applied' do
        set_san_nin
        {
            east: 25_000,
            south: 30_000,
            west: 20_000,
        }.each { |position, score| @game.set_score_for position, score }
        expect(@game.apply_uma!).to be_truthy
        expect(@game.scores).to eq({
                                       east: 25_000,
                                       south: 40_000,
                                       west: 10_000,
                                       north: 0
                                   })
      end
    end
  end

  def set_san_nin
    @game.type = :san_nin
  end
end
