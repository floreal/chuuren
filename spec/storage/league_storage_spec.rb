require 'sqlite3'

describe Chuuren::Storage::LeagueStorage do
  before :each do
    @sqlite = SQLite3::Database.new('db/test.sqlite3')
    @storage = Chuuren::Storage::LeagueStorage.new(@sqlite)
    @league = new_league name: 'Chuuren Potos Regular Rules', game_type: :regular
  end

  after :each do
    @sqlite.execute("DELETE FROM league")
    @sqlite.execute("DELETE FROM sqlite_sequence where name='league'")
    @sqlite.close
  end

  context 'CRUD' do
    it 'stores leagues' do
      expect(@storage.create @league).to eq @league
      expect(@league.id).to eq 1
    end

    it 'does not create a player twice' do
      returned_player = nil
      2.times do
        returned_player = @storage.create @league
      end
      expect(returned_player).to be_nil
    end

    it 'fetches one player' do
      @storage.create @league
      expect(@storage.one(@league.id)).to eq @league
    end

    it 'updates leagues' do
      @storage.create @league
      @league.name = 'Chuuren A-Rules'
      @storage.update @league
      expect(@storage.one(@league.id)).to eq @league
    end

    it 'deletes leagues' do
      @storage.create @league
      @storage.delete(@league.id)
      expect(@storage.one(@league.id)).to be_nil
    end

    it 'fetches several all leagues' do
      @storage.create regular = @league
      @storage.create san_nin = new_league(name: 'Chuuren Potos San-Nin Rules', game_type: :san_nin)
      @storage.create ema = new_league(name: 'EMA Rules', game_type: :regular)

      expect(@storage.all).to eq [regular, san_nin, ema]
    end
  end
  context 'Filter' do

    it 'searches a league' do
      @storage.create regular = @league
      @storage.create san_nin = new_league(name: 'Chuuren Potos San-Nin Rules', game_type: :san_nin)
      @storage.create new_league(name: 'EMA Rules', game_type: :regular)

      expect(@storage.filter("Ch")).to eq [regular, san_nin]
    end
  end

  def new_league(data)
    Chuuren::Models::League.new data
  end
end