require 'sqlite3'

describe Chuuren::Storage::PlayerStorage do
  before :each do
    @sqlite = SQLite3::Database.new('db/test.sqlite3')
    @database = Chuuren::Storage::PlayerStorage.new(@sqlite)
    @player = new_player 'Akagi'
  end

  after :each do
    @sqlite.execute("DELETE FROM player")
    @sqlite.execute("DELETE FROM sqlite_sequence where name='player'")
    @sqlite.close
  end

  context 'CRUD' do
    it 'stores players' do
      expect(@database.create @player).to eq @player
      expect(@player.id).to eq 1
    end

    it 'does not create a player twice' do
      returned_player = nil
      2.times do
        returned_player = @database.create @player
      end
      expect(returned_player).to be_nil
    end

    it 'fetches one player' do
      @database.create @player
      expect(@database.one(@player.id)).to eq @player
    end

    it 'updates players' do
      @database.create @player
      @player.name = 'Tatsuya'
      @database.update @player
      expect(@database.one(@player.id)).to eq @player
    end

    it 'deletes players' do
      @database.create @player
      @database.delete(@player.id)
      expect(@database.one(@player.id)).to be_nil
    end

    it 'fetches several all players' do
      @database.create akagi = @player
      @database.create tatsuya = new_player('Tatsuya')
      @database.create saki = new_player('Saki')

      expect(@database.all).to eq [akagi, tatsuya, saki]
    end
  end
  context 'Filter' do

    it 'searches a player' do
      @database.create akagi = @player
      @database.create new_player 'Tatsuya'
      @database.create saki = new_player('Saki')

      expect(@database.filter("ak")).to eq [akagi, saki]
    end
  end

  def new_player(name)
    Chuuren::Models::Player.new name: name
  end
end