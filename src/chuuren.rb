$:.unshift File.expand_path(File.dirname(__FILE__))

module Chuuren
  module Models
    autoload :League, 'models/league.rb'
    autoload :Player, "models/player.rb"
    autoload :Game,   "models/game.rb"
    autoload :Base,   'models/base.rb'
  end
  module Storage
    autoload :Base,          'storage/base.rb'
    autoload :Crud,          'storage/crud.rb'
    autoload :Filter,        'storage/filter.rb'
    autoload :PlayerStorage, 'storage/player_storage.rb'
    autoload :LeagueStorage, 'storage/league_storage.rb'
  end
  module Controllers
    autoload :PlayerAPI, 'controllers/player_api.rb'
  end
end
