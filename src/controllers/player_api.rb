require 'sinatra'

module Chuuren
  module Controllers
    class PlayerAPI < ::Sinatra::Base
      def initialize
        @storage = settings.storage
      end

      get '/' do
        ::JSON.generate @storage.all
      end

      get '/:id' do |id|
        if (player = @storage.one id).nil?
          404
        else
          ::JSON.generate player
        end
      end

      post '/' do
        if (player = @storage.create(Chuuren::Models::Player::new params )).nil?
          409
        else
          redirect "/player/#{player.id}"
        end
      end

      put '/:id' do |id|
        if (player = @storage.one(id)).nil?
          404
        else
          player.name = params[:name]
          @storage.update(player)
          redirect "/player/#{player.id}"
        end
      end

      delete "/:id" do |id|
        @storage.delete id
        202
      end

      get '/search/:partial_name' do |partial_name|
        ::JSON.generate @storage.filter partial_name
      end
    end
  end
end