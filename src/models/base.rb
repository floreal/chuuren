module Chuuren
  module Models
    class Base
      attr_accessor :id

      def to_json *args
        data = { type: self.class.name.gsub(/^.*::/, '') }
        methods.select { |name| name.to_s =~ /[A-Za-z]+=/ }.each do |method_name|
          attr = method_name.to_s.delete!('=').to_sym
          data[attr] = send attr
        end
        data.to_json args
      end

      def start_persist!
        @persisting = true
      end

      def end_persist!
        @persisting = nil
      end

      def persisting?
        @persisting
      end

      def initialize(hash)
        hash.each do |key, value|
          attr_method = "#{key}=".to_sym
          self.send(attr_method, value) if respond_to? attr_method
        end
      end
    end
  end
end