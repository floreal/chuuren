module Chuuren
  module Models
    class Game < Base
      attr_reader :scores
      attr_accessor :type

      def initialize
        @players = {}
        @scores = {east: 0, south: 0, west: 0, north: 0}
        @type = :regular
      end

      def players
        @players.map { |position, player| [position, player.name] }.to_h
      end

      def register(position, player)
        return false unless position_exists? position
        return false if players.values.include? player.name
        @players[position] = player
        true
      end

      def apply_uma!
        return false if @uma_applied
        uma = san_nin? ? [10_000, 0, -10_000] : [10_000, 5_000, -5_000, -10_000]
        @scores = @scores.sort_by do |score|
          score[1]
        end.reverse!.map do |score|
          [score[0], score[1] + (position_exists?(score[0]) ? uma.shift : 0)]
        end.to_h
        @uma_applied = true
      end

      def who_is_at?(position)
        @players[position].name
      end

      def set_score_for(position, score)
        return false unless position_exists? position
        @scores[position] = score
        true
      end

      def complete?
        expectation = san_nin? ? 75_000 : 100_000
        @scores.values.inject(0) { |sum, score| sum + score } == expectation
      end

      POSITIONS = [:east, :south, :west, :north]

      def san_nin?
        @type == :san_nin
      end

      def position_exists?(position)
        positions = [:east, :south, :west]
        positions << :north unless san_nin?
        return false unless positions.include? position
        true
      end
    end
  end
end
