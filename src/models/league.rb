module Chuuren
  module Models
    class League < Base
      attr_accessor :name, :description, :game_type

      def game_type= game_type
        @game_type = game_type.is_a?(::Symbol)? game_type : [:regular, :san_nin][game_type]
      end

      def game_type for_storage = true
        enum = {regular: 0, san_nin: 1}
        return enum[@game_type] if persisting?
        @game_type
      end

      def to_s
        @name
      end

      def ==(other)
        @id == other.id && @name == other.name
      end
    end
  end
end
