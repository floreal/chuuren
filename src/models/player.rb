module Chuuren
  module Models
    class Player < Base
      attr_accessor :name

      def to_s
        @name
      end

      def ==(other)
        @id == other.id && @name == other.name
      end
    end
  end
end
