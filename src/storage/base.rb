module Chuuren
  module Storage
    class Base
      include Chuuren::Models
      def initialize(db)
        @db = db
      end

      protected

      class << self
        [:table, :fields, :model_type, :search_fields].each do |method_name|
          define_method method_name do |value|
            define_method method_name do
              value
            end
          end
        end
      end
    end
  end
end

