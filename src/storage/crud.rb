module Chuuren
  module Storage
    module Crud
      def create(model)
        begin
          ensure_translate model do
            @db.execute("INSERT INTO #{table} (#{fields.join(', ')}) VALUES (#{fields.map {'?'}.join(', ')})", values(model))
          end
          @db.execute('SELECT last_insert_rowid() AS id') do |row|
            model.id = row.first
          end
          model
        rescue ::SQLite3::ConstraintException
          nil
        end
      end

      def update(model)
        ensure_translate model do
          @db.execute("UPDATE #{table} SET #{fields.map { |field| "#{field} = ?"}.join(', ')} WHERE id = ?", values(model) << model.id)
        end
      end

      def one(id)
        model = nil
        statement = @db.query("SELECT * FROM #{table} WHERE id = ?", [id])

        statement.each_hash do |row|
          model = model_type::new row
        end

        statement.close
        model
      end

      def all
        models = []

        statement = @db.query("SELECT * FROM #{table}")
        statement.each_hash do |row|
          models << model_type::new(row)
        end
        statement.close

        models
      end

      def delete(id)
        @db.execute("DELETE FROM #{table} WHERE id = ?", [id])
        true
      end

      private

      def ensure_translate model
        model.start_persist!
        yield if block_given?
        model.end_persist!
      end

      def values(model)
        fields.map { |field| model.send field }
      end
    end
  end
end