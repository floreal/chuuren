module Chuuren
  module Storage
    module Filter
      def filter(partial_name)
        models = []

        search_fields_coalesed = search_fields.map { |field| "COALESCE(#{field}, '')"}

        statement = @db.query("SELECT * FROM #{table} WHERE #{search_fields_coalesed.join(' || " " || ')} like ?", ["%#{partial_name}%"])
        statement.each_hash do |row|
          models << model_type::new(row)
        end
        statement.close

        models
      end
    end
  end
end