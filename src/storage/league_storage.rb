module Chuuren
  module Storage
    class LeagueStorage < Base
      include Crud
      include Filter

      table :league
      fields [:name, :description, :game_type]
      search_fields [:name, :description]
      model_type League
    end
  end
end